import React from 'react';
import { View, WebView, StatusBar, Button, TouchableOpacity, Text, LayoutAnimation, SafeAreaView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { needBackButton: true }
    this.navigationStateChange = this.navigationStateChange.bind(this);
    this.backButtonPressed = this.backButtonPressed.bind(this);
  }

  navigationStateChange(navState) {
    //console.log("navigationStateChange fired");
    //console.log(navState);
    LayoutAnimation.spring();
    if (!navState.url.includes("www.seriousgmod.com")) {
      this.setState({ needBackButton: true});
      //console.log("Changed state for needBackButton to true.");
    }
    else {
      this.setState({ needBackButton: false});
      //console.log("Changed state for needBackButton to false.");
    }
  }

  backButtonPressed() {
    //console.log("Pressed that fucking button");
    this.refs['webview'].goBack();
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'black'}}>
        <View style={{flex: 1, backgroundColor: 'black'}}>
          <StatusBar hidden={true}/>
          <View style={[!this.state.needBackButton && {display: 'none'}]}>
            <TouchableOpacity onPress={this.backButtonPressed}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                {'     '}
                <Ionicons name="ios-arrow-round-back-outline" size={12}/>
                {' '}Go Back
              </Text>
            </TouchableOpacity>
          </View>
          <WebView
            ref='webview'
            source={{uri: 'https://seriousgmod.com/chat'}}
            style={{flex: 1, backgroundColor: 'rgb(32, 32, 32)'}}
            onNavigationStateChange={this.navigationStateChange}>
          </WebView>
        </View>
      </SafeAreaView>
    );
  }
}
